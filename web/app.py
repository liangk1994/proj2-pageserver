from flask import Flask, render_template
import os


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('trivia.html')

@app.route('/<sinput>/')
def pages(sinput):
    if os.path.isfile("templates/" + sinput) == True:
        return render_template(str(sinput))
    elif ".." in sinput or "//" in sinput or "~" in sinput:
    	return render_template('403.html')
    elif os.path.isfile(sinput) != True:
    	return render_template('404.html')
    # else:
    # 	return str(sinput)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
